# Utilisez l'image de base Python pour Flask
FROM python:3.9

# Définissez le répertoire de travail
WORKDIR /app

# Copiez les fichiers nécessaires dans le conteneur
COPY app.py .
COPY templates templates

# Installez les dépendances Flask
RUN pip install flask

# Exposez le port 5000 (port par défaut pour Flask)
EXPOSE 5000

# Commande pour exécuter l'application Flask
CMD ["python", "app.py"]

