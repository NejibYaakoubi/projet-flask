
**Étape 1 : Configuration du Serveur Ubuntu**

1. Installez Python 3 :


```
sudo apt update
sudo apt install python3
```


2. Installez pip :



`sudo apt install python3-pip`


3. Installez Flask :
 

`pip3 install flask` 

É**tape 2 : Copie du Code source sur le Serveur**

1. Copiez les fichiers nécessaires sur le serveur :

2. Copiez le fichier app.py et le répertoire templates sur le serveur.

**Étape 3 : Exécution de l'Application Flask**

1. Exécutez l'application Flask :



`python3 app.py`

2. Accédez à l'application via le navigateur :

Ouvrez un navigateur et accédez à http://adresse_ip_du_serveur:5000.

