from flask import Flask, render_template
import os

app = Flask(__name__)

@app.route('/')
def index():
    # Obtenez la couleur à partir de la variable d'environnement, sinon utilisez une valeur par défaut
    couleur_fond = os.environ.get('COULEUR_FOND', 'lightblue')
    return render_template('index.html', couleur_fond=couleur_fond)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)

