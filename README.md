**Construction et exécution du conteneur Docker**
1.  Construisez l'image Docker à partir du répertoire contenant le Dockerfile : 

`docker build -t flask-color-app .`

2. Exécutez le conteneur en spécifiant la couleur de fond souhaitée (par exemple, lightgreen) : 

`docker run -p 5000:5000 -e COULEUR_FOND=lightgreen flask-color-app`

Le site devrait être accessible à l'adresse http://localhost:5000 avec la couleur de fond spécifiée. Vous pouvez changer la couleur en modifiant la valeur de la variable d'environnement COULEUR_FOND lors de l'exécution du conteneur Docker.

